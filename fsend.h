#include<sys/types.h>
#include<inttypes.h>

#define FSEND_INIT      0x01
#define FSEND_LOGIN     0x02
#define FSEND_SFILE     0x03
#define FSEND_SDIR      0x04
#define FSEND_ACK       0x05
#define FSEND_ERR       0x06
#define FSEND_END       0x07

#define FSEND_PORT      8080

/* struct fsend request should be 65536 kilobytes */
#define FSEND_PRE_SIZE          9
#define FSEND_BODY_SIZE         65527
#define FSEND_FILENAME_SIZE     32700
#define FSEND_FULLPATH_SIZE     32700
#define FSEND_FILESSTR_SIZE     127
#define FSEND_USERNAM_SIZE      128
#define FSEND_PASSWRD_SIZE      128

#define connected(handle) (handle->sockfd == -1 ? 0 : 1)
#define loggedin(handle) (handle->login)

struct fsend {
    char username[FSEND_USERNAM_SIZE];
    char password[FSEND_PASSWRD_SIZE];
    uid_t uid;
    gid_t gid;
    int login;
    int sockfd;
    char addr[64];
};

struct __attribute__((__packed__)) fsend_request {
    uint8_t preamble[FSEND_PRE_SIZE];
    uint8_t code;
    uint8_t body[FSEND_BODY_SIZE];
};

typedef struct fsend fsend_t;

fsend_t *fsend_init();
int fsend_connect(fsend_t *handle, char *address);
int fsend_disconnect(fsend_t *handle);
int send_fsend_req(fsend_t *handle, struct fsend_request *req);
int recv_fsend_req(fsend_t *handle, struct fsend_request *req);
int send_fsend_ack(fsend_t *handle, char *body);
int send_fsend_err(fsend_t *handle, char *body);
int recv_fsend_ack(fsend_t *handle, char *body, int size);
int fsend_end(fsend_t *handle, char *body);
void fsend_package(struct fsend_request *req, uint8_t code, int lines, ...);
long fsend_sendfile(fsend_t *handle, char *source, char *dest);
long fsend_recvfile(fsend_t *handle, struct fsend_request *req);
int fsend_senddir(fsend_t *handle, char *dest);
int fsend_recvdir(fsend_t *handle, struct fsend_request *req);
int fsend_client_login(fsend_t *handle, char *username, char *password);
int fsend_server_login(fsend_t *handle, struct fsend_request *req);
void fsend_handle_req(fsend_t *handle, struct fsend_request *req);
