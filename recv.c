#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/stat.h>
#include<netdb.h>
#include<unistd.h>
#include<pwd.h>
#include<string.h>
#include"recv.h"

long recvbuffer(int sockfd, uint8_t *buffer, unsigned long bufferlen)
{
    unsigned long r, received = 0;
    
    while(bufferlen > 0) {

        if((r = recv(sockfd, buffer, bufferlen, 0)) <= 0)
            return r;
        else {
            buffer += r;
            bufferlen -= r;
            received += r;
        }
    }

    return received;
}

long recvstring(int sockfd, char *str)
{
    char c = 1;
    long i = 0;
    long received = 0;

    while(c != '\0') {
        int r = recv(sockfd, &c, 1, 0);
        if(r > 0) {
            str[i++] = c;
            received += r;
        }
        else
            return r;
    }

    return received;
}

long recvfile(int sockfd, char* username)
{
    /* First recv the file name and file size */
    char str[2048];
    if(recvstring(sockfd, str) == -1)
        return -1;

    char filename[2048];
    strcpy(filename, strtok(str, "\t"));
    char filesizestr[128];
    strcpy(filesizestr, strtok(NULL, "\t"));

    unsigned long filesize = atol(filesizestr);

    uint8_t *buffer = (uint8_t*) malloc(RECV_BUFF_LEN);
    
    FILE *fd;
    if((fd = fopen(filename, "w")) == NULL)
            return -1;

    unsigned long fsize = filesize, r, byteswriten = 0;

    while(byteswriten < filesize) {
        if((r = recvbuffer(sockfd, buffer, min(RECV_BUFF_LEN, fsize))) <= 0)
                return r;

        r = fwrite(buffer, sizeof(uint8_t), r, fd);
        fsize -= r;
        byteswriten += r;
    }

    /* Get the uid and gid of the user by looking for them in
     * the passwd file */
    struct passwd *pass = getpwnam(username);

    /* If pass is NULL then the user doesn't exist in the passwd
     * file, if chown does not return 0 then a problem occured and
     * errno has been set */
    if(pass) {
        if(chown(filename, pass->pw_uid, pass->pw_gid))
            return -1;
    }
    else
        return -1;

    fclose(fd);
    free(buffer);

    return byteswriten;
}

