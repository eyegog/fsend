#include<stdint.h>

/* Buffer size of receiving is 50Mb */
#define RECV_BUFF_LEN 52428800

#ifndef max
#define max(a, b) (a > b ? a : b)
#endif
#ifndef min
#define min(a, b) (a < b ? a : b)
#endif

long recvbuffer(int sockfd, uint8_t *buffer, unsigned long bufferlen);
long recvstring(int sockfd, char *str);
long recvfile(int sockfd, char *username);
