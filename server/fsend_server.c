#include<stdio.h>
#include<stdlib.h>
#include<netdb.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<string.h>
#include<stdint.h>
#include<errno.h>
#include<pthread.h>
#include<signal.h>
#include"../send.h"
#include"../recv.h"
#include"../valid.h"
#include"../fsend.h"

int main(int argc, char *argv[])
{
    signal(SIGPIPE, SIG_IGN);

    struct sockaddr_in hints;
    memset(&hints, 0, sizeof(hints));

    hints.sin_family = AF_INET;
    hints.sin_port = htons(8080);
    hints.sin_addr.s_addr = INADDR_ANY;

    int sfd = socket(hints.sin_family, SOCK_STREAM, 0);
         
    if(bind(sfd, (struct sockaddr *) &hints, sizeof(hints)) != 0) {
        fprintf(stderr, "Cannot bind(): %s\n", strerror(errno));
        exit(-1);
    }
    
    /*
    struct addrinfo hints;
    memset(&hints, sizeof(hints), 0);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    struct addrinfo *bind_address, *ptr;
    getaddrinfo(0, "8080", &hints, &bind_address);
    int sfd;
    
    for(ptr = bind_address; ptr; ptr = ptr->ai_next) {
        sfd = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if(sfd != -1)
            if(bind(sfd, ptr->ai_addr, ptr->ai_addrlen) == 0)
                break;
            else
                close(sfd);
    }

    if(!bind_address) {
        fprintf(stderr, "No devices available: %s\n", strerror(errno));
        exit(-1);
    }

    */
   for(;;) {
        if(!listen(sfd, 10)) {
            struct sockaddr_in client_addr;
            socklen_t client_size = sizeof(client_addr);
            int clientfd = accept(sfd, (struct sockaddr *) &client_addr, &client_size);

            void *handle_req(void *args);

            char addr[INET_ADDRSTRLEN];
            struct in_addr caddr = client_addr.sin_addr;
            if(inet_ntop(AF_INET, &caddr, addr, INET_ADDRSTRLEN) == NULL) {
                fprintf(stderr, "Error in inet_ntop(): %s\n", strerror(errno));
                strcpy(addr, "");
            }

            char *args[2];
            args[0] = (char *) &clientfd;
            args[1] = addr;

            pthread_t tid;
            pthread_create(&tid, NULL, handle_req, args);
            printf("Opened connection: %s\n", addr);
        }
        else {
            fprintf(stderr, "Error in listen(): %s\n", strerror(errno));
            close(sfd);
            exit(-1);
        }
    }
    close(sfd);
    return 0;
}

void *handle_req(void *args)
{
    char **cargs = (char **) args;
    int sockfd = (int) *(cargs[0]);
    char *addr = cargs[1];
    
    fsend_t *handle = fsend_init();
    handle->sockfd = sockfd;
    strcpy(handle->addr, addr);
    struct fsend_request req;

    while(connected(handle)) {
        recv_fsend_req(handle, &req);
        fsend_handle_req(handle, &req);
    }

    free(handle);
    pthread_exit(NULL);
}
