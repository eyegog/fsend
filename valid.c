#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<crypt.h>
#include<pwd.h>
#include<shadow.h>
#include<stdio.h>
#include<errno.h>
#include<string.h>

/* returns 1 if username and password is valid
 * 0 otherwise
 */
int valid_user(const char *username, const char *password)
{
    struct passwd *user_pass = getpwnam(username);

    /* If user_pass is NULL then the username doesn't exist
     * in the passwd file */
    if(!user_pass) {
        fprintf(stderr, "Error: User does not exist in /etc/passwd\n");
        return 0;
    }

    int result = 0;

    /* If the password in passwd is "x" then the password is
     * encrypted and is located in the shadowfile */
    if(!strcmp(user_pass->pw_passwd, "x")) {
        struct spwd *user_shadow = getspnam(username);
        
        /* If user_pass is NULL then the username doesn't exist
         * in the shadow file */
        if(!user_shadow) {
            fprintf(stderr, "Error: User does not exist in /etc/shadow\n");
            return 0;
        }

        if(!strcmp(user_shadow->sp_pwdp, crypt(password, user_shadow->sp_pwdp)))
            result = 1;
    } 
    else
        if(!strcmp(user_pass->pw_passwd, crypt(password, user_pass->pw_passwd)))
            result = 1;

    return result;
}
